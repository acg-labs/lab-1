﻿namespace Lab1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Draw = new System.Windows.Forms.Button();
            this.valueA = new System.Windows.Forms.TextBox();
            this.valueB = new System.Windows.Forms.TextBox();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Draw
            // 
            this.Draw.Location = new System.Drawing.Point(335, 36);
            this.Draw.Name = "Draw";
            this.Draw.Size = new System.Drawing.Size(101, 23);
            this.Draw.TabIndex = 0;
            this.Draw.Text = "Draw";
            this.Draw.UseVisualStyleBackColor = true;
            this.Draw.Click += new System.EventHandler(this.Draw_Click);
            // 
            // valueA
            // 
            this.valueA.Location = new System.Drawing.Point(75, 38);
            this.valueA.Name = "valueA";
            this.valueA.Size = new System.Drawing.Size(100, 20);
            this.valueA.TabIndex = 1;
            this.valueA.Text = "40";
            // 
            // valueB
            // 
            this.valueB.Location = new System.Drawing.Point(211, 38);
            this.valueB.Name = "valueB";
            this.valueB.Size = new System.Drawing.Size(100, 20);
            this.valueB.TabIndex = 2;
            this.valueB.Text = "120";
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(53, 41);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(17, 13);
            this.labelA.TabIndex = 3;
            this.labelA.Text = "A:";
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(188, 41);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(17, 13);
            this.labelB.TabIndex = 4;
            this.labelB.Text = "B:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 103);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.valueB);
            this.Controls.Add(this.valueA);
            this.Controls.Add(this.Draw);
            this.Name = "MainForm";
            this.Text = "Lab 1: Epicycloid";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Draw;
        private System.Windows.Forms.TextBox valueA;
        private System.Windows.Forms.TextBox valueB;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
    }
}

