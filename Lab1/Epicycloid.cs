﻿using System;
using System.Drawing;

namespace Lab1
{
    public class Epicycloid
    {
        private const int NumberOfPoints = 36000;

        private int _a;

        private int _b;

        public Epicycloid(int a, int b)
        {
            _a = a;
            _b = b;
        }

        public Point[] GetPoints()
        {
            Point[] points = new Point[NumberOfPoints];

            for (var i = 0; i < NumberOfPoints; i++)
            {
                points[i].X = (int)GetX(_a, _b, i);
                points[i].Y = (int)GetY(_a, _b, i);
            }

            return points;
        }

        private double GetX(int a, int b, double f)
        {
            return (a + b) * Math.Cos(f) - a * Math.Cos((a + b) * f / a);
        }

        private double GetY(int a, int b, double f)
        {
            return (a + b) * Math.Sin(f) - a * Math.Sin((a + b) * f / a);
        }
    }
}
