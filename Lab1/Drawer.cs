﻿using System;
using System.Drawing;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Primitives;
using System.Windows.Forms;

namespace Lab1
{
    public class Drawer
    {
        private const int Width = 1000;

        private const int Height = 700;

        public Drawer()
        {
            Video.SetVideoMode(Width, Height);
            Video.WindowCaption = "Lab1";
        }

        public void Draw(Point[] points)
        {        
            Video.Screen.Blit(DrawEpicycloid(points));
            Video.Screen.Update();
        }

        private Surface DrawEpicycloid(Point[] points)
        {
            Surface surface = new Surface(Video.Screen.Size);

            foreach (var point in points)
            {
                surface.Draw(new Circle(SetOffset(point), 1), Color.Purple, true, true);
            }

            return surface;
        }

        private Point SetOffset(Point point)
        {
            return new Point(point.X + Width / 2, point.Y + Height / 2);
        }
    }
}
