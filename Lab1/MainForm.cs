﻿using System;
using System.Windows.Forms;

namespace Lab1
{
    public partial class MainForm : Form
    {
        private const int A = 50;
        private const int B = 150;
        private Drawer _drawer;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Draw_Click(object sender, EventArgs e)
        {
            if (int.TryParse(valueA.Text, out int a) && int.TryParse(valueB.Text, out int b) &&
                a > 0 && b > 0)
            {
                if (_drawer == null)
                {
                    InitializeDrawer();
                }

                DrawEpicycloid(a, b);
            }
            else
            {
                MessageBox.Show("Check values.");
            }
        }

        private void InitializeDrawer()
        {
            _drawer = new Drawer();
        }

        private void DrawEpicycloid(int a, int b)
        {
            Epicycloid epicycloid = new Epicycloid(a, b);
            _drawer.Draw(epicycloid.GetPoints());
        }
    }
}
